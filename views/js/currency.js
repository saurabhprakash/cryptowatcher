function getCurrencyData(currencyCode, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            return callback(JSON.parse(this.responseText));
        }
    };
    xhttp.open("GET", "/latest?currencyCode="+currencyCode);
    xhttp.send();
}

function getPastCurrencyData(currencyCode, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            return callback(JSON.parse(this.responseText));
        }
    };
    xhttp.open("GET", "/historic/history?currencyCode="+currencyCode);
    xhttp.send();
}

function parseQueryString (url) {
    var urlParams = {};
    url.replace(
        new RegExp("([^?=&]+)(=([^&]*))?", "g"),
        function($0, $1, $2, $3) {
            urlParams[$1] = $3;
        }
    );

    return urlParams;
};

function customSort(data) {
    data.sort(function (l, r) {
        return l[0] - r[0];
    });
};

function populateHighCharts(divId, divText, data, currencyCode, data_map) {
    customSort(data);
    if (divId == 'container') $('#headline').html("<h2>" + currencyCode + " trading at <b>" + data[data.length-1][1] + "</b> USD right now.!</h2>");

    Highcharts.chart(divId, {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: divText
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: 'Trading at:'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b> traded at <b>' + this.y + ' USD</b> on <b>' + new Date(this.x).toUTCString() + '</b> with <b>' + data_map[this.x.toString()+"_"+this.y.toString()] + '</b> volume.';
            }
        },
        series: [{
            type: 'area',
            name: currencyCode,
            data: data
        }]
    });
}

function fetchTodaysTrend(currencyCode) {
    getCurrencyData(currencyCode, function (result) {
        var data = [];
        var data_map = {};
        var temp = result[0]["Time Series (Digital Currency Intraday)"];
        for (var prop in temp) {
            if (temp.hasOwnProperty(prop)) {
                var x = Date.parse(prop + " UTC");
                if (x < new Date().getTime()-24*60*60*1000) continue;
                var y = parseFloat(temp[prop]["1b. price (USD)"]);
                data.push([x, y]);
                data_map[x.toString()+"_"+y.toString()] = temp[prop]["2. volume"];
            }
        }

        populateHighCharts('container', "Today's trading numbers: ", data, currencyCode, data_map);

    });
}

$(document).ready(function () {
    var currencyCode = parseQueryString(location.search).currencyCode;
    document.title += " - " + currencyCode;
    fetchTodaysTrend(currencyCode);
    setInterval(function () {
        fetchTodaysTrend(currencyCode);
    }, 2*60*1000);

    getPastCurrencyData(currencyCode, function (result) {
        var data = [];
        var data_map = {};
        for (var i=0; i<result.length; i++) {
            var x = new Date(result[i].date).getTime();
            var y = result[i].price;
            data.push([x, y]);
            data_map[x.toString()+"_"+y.toString()] = result[i]["txVolume"];
        }

        populateHighCharts('container2', "Past trading numbers: ", data, currencyCode, data_map);
    });
});