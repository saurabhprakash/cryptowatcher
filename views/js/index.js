function getAllCurrencyData(callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            return callback(JSON.parse(this.responseText));
        }
    };
    xhttp.open("GET", "/historic");
    xhttp.send();
}

$(document).ready(function () {
    getAllCurrencyData(function (data) {
        for (var i=0; i<data.length; i++) {
            var currencyDetail = data[i][0];
            var li = document.createElement('li');
            li.className = "list-group-item";
            li.innerHTML = '<a href="/currency.html?currencyCode=' + currencyDetail.currency + '">'
                + currencyDetail.currency + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                + currencyDetail.price + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                + currencyDetail.txVolume + '</a>'
            $('#currencyContainer').append(li);
        }
    });
});