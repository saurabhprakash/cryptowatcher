var express = require("express");
var db = require("./config/DBConfig");
var app = express();

app.get("/", function (req, res) {
    if (!!req.query.currencyCode) return res.redirect("./currency.html?currencyCode=" + req.query.currencyCode);
    return res.redirect("./index.html");
});

app.use("/historic", require("./controllers/historicCryptoDataController"));
app.use("/latest", require("./controllers/latestCryptoDataController"));

app.use(express.static("views"));
app.use(express.static("views/markup"));

db.connect(process.env.DATABASE || "LOCAL", function () {
    console.log("DB Connected. Starting App.");
    app.listen(process.env.PORT || 3000, function() {
        console.log("App Started. Fire me some requests.")
    });
});