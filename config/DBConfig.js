var sql = require("mysql");
var databases = require("./Constants").databases;
var pool;

var getPool = function () {
    return pool;
};

var connect = function(mode, callback) {
    pool = sql.createPool(databases.env[mode]);
    callback();
};

module.exports = {
    getPool: getPool,
    connect: connect
};