var historicCryptoDataLoader = require("../models/historicCryptoDataLoader");
var router = require("express").Router();

router.get("/", function (req, res) {
    historicCryptoDataLoader.getLatestPrice(function (error, response) {
        if (error) return res.status(500).json(response);
        return res.status(200).json(response);
    });
});

router.get("/history", function (req, res) {
    historicCryptoDataLoader.getHistoricDataOfCurrency(req.query.currencyCode, function (error, response) {
        if (error) return res.status(500).json(response);
        return res.status(200).json(response);
    });
});

module.exports = router;