var latestCryptoDataLoader = require("../models/latestCryptoDataLoader");
var router = require("express").Router();

router.get("/", function(req, res) {
    var currencyCode = req.query.currencyCode;
    var marketCode = req.query.marketCode || "USD";
    latestCryptoDataLoader.getTimeSeriesCryptoData(currencyCode, marketCode, function (error, response) {
        if (error) return res.status(500).json(response);
        res.setHeader('Cache-Control', 'public, max-age=' + 2*60*60);
        return res.status(200).json(response);
    });
});

module.exports = router;