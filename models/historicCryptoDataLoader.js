var db = require("../config/DBConfig");
var currencies = require("../config/Constants.json").crypto.currencies;
var table = require("../config/Constants.json").databases.table;
var async = require("async");

var generateTopQuery = function (currency) {
    return {
        selectQuery: "SELECT price, txVolume, currency FROM " + table + " WHERE currency=? ORDER BY DATE DESC LIMIT 1",
        currency: currency
    };
};

var generateQuery = function (currency) {
    return {
        selectQuery: "SELECT price, txVolume, currency, date FROM " + table + " WHERE currency=? ORDER BY DATE",
        currency: currency
    }
};

var getLatestPrice = function (callback) {
    var queries = [];
    for (var i=0; i<currencies.length; i++) {
        queries.push(generateTopQuery(currencies[i]));
    }
    async.map(queries, function (item, cb) {
        db.getPool().query(item.selectQuery, [item.currency], function (error, res) {
            if (error) return cb(null, {
                "status": "DOWN",
                "message": "Backend api down. Contact api-owner."
            });
            return cb(null, res);
        });
    }, function (err, results) {
        if (err) return callback(null, {
            "status": "DOWN",
            "message": "Backend api down. Contact api-owner."
        });
        return callback(null, results);
    });
};

var getHistoricDataOfCurrency = function (currencyCode, callback) {
    var queryObject = generateQuery(currencyCode);
    db.getPool().query(queryObject.selectQuery, [queryObject.currency], function (error, res) {
        if (error) return callback(error, {
            "status": "DOWN",
            "message": "Backend api down. Contact api-owner."
        });
        return callback(null, res);
    });
};

module.exports = {
    getLatestPrice: getLatestPrice,
    getHistoricDataOfCurrency: getHistoricDataOfCurrency
};