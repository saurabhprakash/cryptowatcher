var request = require("request");
var api = require("../config/Constants.json").crypto.latest.api;
var currencies = require("../config/Constants.json").crypto.currencies;
var async = require("async");

var getThirdPartyApiRequestUrl = function (currencyCode, marketCode) {
    return api.baseUrl
        + "?function=" + api.function
        + "&apikey=" + api.apikey
        + "&symbol=" + currencyCode
        + "&market=" + marketCode;
};

var getTimeSeriesCryptoData = function(currencyCode, marketCode, callback) {
    var urls = [];
    if (!currencyCode) {
        for (var i=0; i<currencies.length; i++) {
            urls.push(getThirdPartyApiRequestUrl(currencies[i], marketCode));
        }
    } else urls.push(getThirdPartyApiRequestUrl(currencyCode, marketCode));

    return async.map(urls, function (url, cb) {
        request(url, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                return cb(null, JSON.parse(body));
            }
            // specifically passing first argument (is error argument) as null so that remaining requests complete
            return cb(null, {
                "status": "DOWN",
                "message": "Backend api down. Contact api-owner."
            });
        });
    }, function (err, results) {
        if (err) {
            return callback(err, {
                "status": "DOWN",
                "message": "Backend api down. Contact api-owner."
            });
        }
        return callback(null, results);
    });
}

module.exports = {
    getTimeSeriesCryptoData: getTimeSeriesCryptoData
};